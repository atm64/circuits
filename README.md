Graphisme "circuits" utilisé dans les visuels ATM64

Dérivation de " [Electronic Circuit](https://openclipart.org/detail/99217/electronic-circuit) " par [KUBA](https://openclipart.org/artist/kuba)

[CC0 - Domaine Public](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) / [Circuits with leaves](https://openclipart.org/detail/340894/circuit-with-leaves) - ATM64 2023 / contact@atm64.fr